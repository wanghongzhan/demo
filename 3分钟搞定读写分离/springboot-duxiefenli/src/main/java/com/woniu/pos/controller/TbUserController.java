package com.woniu.pos.controller;

import com.woniu.pos.common.dao.entity.User;
import com.woniu.pos.common.dao.repository.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 3分钟搞定读写分离
 */
@RestController
@RequestMapping("/user")
public class TbUserController {

    @Autowired
    private UserMapper userMapper;

    @GetMapping("/userList")
    public List<User> selectUsers() {
        return userMapper.selectList(null);
    }

    @PostMapping("/insertUser")
    public void insertUser(@RequestBody User user) {
        userMapper.insert(user);
    }

}