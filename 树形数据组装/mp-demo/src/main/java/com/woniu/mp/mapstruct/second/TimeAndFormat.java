package com.woniu.mp.mapstruct.second;
import lombok.Data;

import java.util.Date;

@Data
public class TimeAndFormat {
    private Date time;
    private String format;

    public TimeAndFormat(Date time, String format) {
        this.time = time;
        this.format = format;
    }

}
