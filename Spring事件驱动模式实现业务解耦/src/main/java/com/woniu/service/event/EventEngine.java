package com.woniu.service.event;

/**
 * @className: EventEngine
 * @author: woniu
 * @date: 2023/4/8
 **/
public interface EventEngine {
    /**
     * 发送事件
     *
     * @param event 事件
     */
    void publishEvent(BizEvent event);
}
