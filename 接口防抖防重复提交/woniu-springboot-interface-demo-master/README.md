# summo-springboot-interface-demo

#### 介绍
《优化接口设计的思路》系列文章演示用demo

#### 软件架构
大家好！我是sum墨，一个一线的底层码农，平时喜欢研究和思考一些技术相关的问题并整理成文，限于本人水平，如果文章和代码有表述不当之处，还请不吝赐教。
作为一名从业已达六年的老码农，我的工作主要是开发后端Java业务系统，包括各种管理后台和小程序等。在这些项目中，我设计过单/多租户体系系统，对接过许多开放平台，也搞过消息中心这类较为复杂的应用，但幸运的是，我至今还没有遇到过线上系统由于代码崩溃导致资损的情况。这其中的原因有三点：一是业务系统本身并不复杂；二是我一直遵循某大厂代码规约，在开发过程中尽可能按规约编写代码；三是经过多年的开发经验积累，我成为了一名熟练工，掌握了一些实用的技巧。


#### 安装教程

1.  安装maven
2.  安装git
3.  安装idea或eclipse
4.  安装MySQL
5.  执行建表语句和数据插入SQL
6.  更新依赖


#### 使用说明

[《优化接口设计的思路》系列：第一篇—接口参数的一些弯弯绕绕](https://juejin.cn/post/7278246015194611727)

[《优化接口设计的思路》系列：第二篇—接口用户上下文的设计与实现](https://juejin.cn/post/7278597227785437195)

[《优化接口设计的思路》系列：第三篇—留下用户调用接口的痕迹](https://juejin.cn/spost/7280006996585381948)

[《优化接口设计的思路》系列：第四篇—接口的权限控制](https://juejin.cn/spost/7282666723915317285)

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
