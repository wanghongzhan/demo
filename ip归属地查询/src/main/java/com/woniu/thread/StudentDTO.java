package com.woniu.thread;


import com.alibaba.fastjson.annotation.JSONField;

public class StudentDTO {
    private String studentName;

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }
    @JSONField(serialize = false)
    public Boolean isChinaName() {
        return this.studentName.equals("孔子");
    }
}

